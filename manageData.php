<?php

const USERNAME = "jesori";
const PASSWORD = "f7c5";
const ADDRESS = 'mysql:host=db.mkalmo.xyz;dbname=jesori';

function getItems() {

    $connection = new PDO(ADDRESS, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $connection->prepare(
        'SELECT id, firstName, lastName, group_concat(phones.phone ORDER BY phones.id_p) phone
                FROM contacts
                LEFT JOIN phones ON contacts.id = phones.person_id GROUP BY contacts.id');
    $stmt->execute();

    $personData = [];

    foreach ($stmt as $row) {
        $id = $row['id'];
        $firstName = $row['firstName'] != null ? $row['firstName'] : "No Data";
        $lastName = $row['lastName'] != null ? $row['lastName'] : "No Data";
        $phone = $row['phone'] != null ? $row['phone'] : "No Data";

        $personData[] = array('id' => $id, 'firstName' => $firstName,
            'lastName' => $lastName,
            'phone' => $phone);
    }

    $connection = null;
    return $personData;
}

function addData($firstName, $lastName, $phones) {

    $connection = new PDO(ADDRESS, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $connection->prepare(
        'INSERT INTO contacts (firstname, lastName) VALUES (:firstName,:lastName)');
    $stmt->bindValue(':firstName', $firstName);
    $stmt->bindValue(':lastName', $lastName);

    $stmt->execute();

    $id = (int)$connection->lastInsertId();

    foreach ($phones as $phone) {
        if ($phone != null) {
            $stmt = $connection->prepare('INSERT
                INTO phones (person_id, phone) VALUES (:person_id, :phone )');
            $stmt->bindValue(':person_id', $id);
            $stmt->bindValue(':phone', $phone);
            $stmt->execute();
        }
    }
    $connection = null;
}

function editData($firstName, $lastName, $phones, $id) {
    $connection = new PDO(ADDRESS, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $connection->prepare(
        "UPDATE contacts SET firstName='$firstName', lastName='$lastName' WHERE id='$id'");
    $stmt->execute();

    $stmt = $connection->prepare(
        "SELECT * FROM phones");
    $stmt->execute();
    $phoneId= [];
    foreach ($stmt as $phone){
        if ($phone['person_id'] === $id){
            $phoneId[] = $phone['id_p'];
        }
    }

    $index = 0;

    foreach ($phoneId as $idp) {
        $stmt = $connection->prepare(
            "UPDATE phones SET phone='$phones[$index]' WHERE id_p='$idp'");
        $stmt->execute();
        $index++;
    }
    $connection = null;
}

function checkDataForErrors($firstName, $lastName) {
    $errors = [];
    if (strlen($firstName) < 2){
        $errors[] = "Eesnimi peab olema 2 kuni 50 märki!";
    }
    if (strlen($lastName) < 2){
        $errors[] = "Perekonnanimi peab olema 2 kuni 50 märki!";
    }
    return $errors;
}