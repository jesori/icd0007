<?php

require_once 'vendor/tpl.php';
require_once 'Request.php';
require_once 'manageData.php';


$request = new Request($_REQUEST);

//print $request; // display input parameters (for debugging)
$cmd = $request->param('cmd')
    ? $request->param('cmd')
    : 'list_page';
$id = $request->param('id') ? $request->param('id') : null;

if ($cmd === 'list_page') {
    echo $id;
    $person = getItems();
    $data = ['Items' => $person,
        'template' => 'list.html'];

    print renderTemplate('main.html', $data);
} else if ($cmd === 'add_page') {
    $data = [
        'title' => 'Lisa',
        'template' => 'add.html',
        'errors' => []
    ];

    print renderTemplate('main.html', $data);
} else if ($cmd === 'save') {
    $errors = checkDataForErrors($_POST['firstName'], $_POST['lastName']);
    $phones = [$_POST['phone1'], $_POST['phone2'], $_POST['phone3']];
    if (count($errors) > 0){
        $data = [ 'template' => 'add.html', 'firstName' => $_POST['firstName'],
            'lastName' => $_POST['lastName'],
            'phone' => $phones,
            'errors' => $errors];
        print renderTemplate('main.html', $data);
    }else{

        addData($_POST['firstName'], $_POST['lastName'], $phones);

        header("Location: ?cmd=list_page");
    }
} else if ($cmd === 'edit') {
    $errors = checkDataForErrors($_POST['firstName'], $_POST['lastName']);
    $phones = [$_POST['phone1'], $_POST['phone2'], $_POST['phone3']];
    if (count($errors) > 0){
        $personData = ['id' => $id, 'firstName' => $_POST['firstName'],
            'lastName' => $_POST['lastName'],
            'phone' => $phones];
        $data = [ 'template' => 'edit.html', 'Person' => $personData,
            'errors' => $errors];
        print renderTemplate('main.html', $data);
    }else{

        editData($_POST['firstName'], $_POST['lastName'], $phones, $id);

        header("Location: ?cmd=list_page");
    }
} else if ($cmd === 'change_person_data') {
    $connection = new PDO(ADDRESS, USERNAME, PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $connection->prepare(
        'SELECT id, firstName, lastName, group_concat(phones.phone ORDER BY phones.id_p) phone
                FROM contacts
                LEFT JOIN phones ON contacts.id = phones.person_id GROUP BY contacts.id');
    $stmt->execute();

    foreach ($stmt as $row) {
        if ($row['id'] === $id) {
            $firstName = $row['firstName'] != null ? $row['firstName'] : null;
            $lastName = $row['lastName'] != null ? $row['lastName'] : null;
            $phones = $row['phone'] != null ? $row['phone'] : null;
            if ($phones !== null) {
                $phones = explode(",", $phones);
            }

            $personData = ['id' => $id, 'firstName' => $firstName,
                'lastName' => $lastName,
                'phone' => $phones];
        }
    }
    $connection = null;
    $data = ['Person' => $personData,
        'template' => 'edit.html'];

    print renderTemplate('main.html', $data);
}

